package com.testmodyo.getpokemon;

public interface EntityMockeable<T> {
	public T generate();
}