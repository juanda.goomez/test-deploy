package com.testmodyo.getpokemon.controller.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.models.dto.ResponseDto;

import lombok.extern.slf4j.Slf4j;

@Component
@ControllerAdvice
@Slf4j
public class ExceptionController {

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ResponseDto<String>> handleError(HttpServletRequest req, Exception e) {
		log.error(CommonConstants.DEFAULT_REQUEST + req.getRequestURL() + CommonConstants.DEFAULT_RAISED, e);
		ResponseDto<String> exceptionResponseData = new ResponseDto<>();
		exceptionResponseData.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		exceptionResponseData.setDescription(e.getMessage());
		exceptionResponseData.setCode(CommonConstants.APPLICATION_ERROR);
		return new ResponseEntity<>(exceptionResponseData, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ResponseDto<String>> handleMethodArgumentNotValid(HttpServletRequest req, Exception e) {
		log.error(CommonConstants.DEFAULT_REQUEST + req.getRequestURL() + CommonConstants.DEFAULT_RAISED, e);
		ResponseDto<String> exceptionResponseData = new ResponseDto<>();
		exceptionResponseData.setStatus(HttpStatus.BAD_REQUEST.value());
		exceptionResponseData.setDescription(e.getMessage());
		exceptionResponseData.setCode(CommonConstants.INVALID_REQUEST);
		return new ResponseEntity<>(exceptionResponseData, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = BusinessException.class)
	public ResponseEntity<ResponseDto<String>> handleBusinessException(HttpServletRequest req, BusinessException e) {
		log.error(CommonConstants.DEFAULT_REQUEST + req.getRequestURL() + CommonConstants.DEFAULT_RAISED, e);
		ResponseDto<String> exceptionResponseData = new ResponseDto<>();
		exceptionResponseData.setStatus(e.getStatus());
		exceptionResponseData.setDescription(e.getDescription());
		exceptionResponseData.setCode(e.getCode());
		return new ResponseEntity<>(exceptionResponseData, HttpStatus.valueOf(e.getStatus()));
	}

}
