package com.testmodyo.getpokemon.controller.exception;

import lombok.Data;

@Data
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;

	private String code;
	private String description;
	private int status;

	public BusinessException(String code, String description, int status) {
		super();
		this.code = code;
		this.description = description;
		this.status = status;
	}

}
