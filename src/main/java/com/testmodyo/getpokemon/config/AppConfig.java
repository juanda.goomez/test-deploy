package com.testmodyo.getpokemon.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.testmodyo.getpokemon.common.CommonConstants;

import lombok.Generated;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Generated
@Configuration
public class AppConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage(CommonConstants.DEFAULT_BASE_PACKAGE))
				.paths(PathSelectors.any()).build();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
