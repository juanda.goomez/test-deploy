package com.testmodyo.getpokemon.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;
import com.testmodyo.getpokemon.utils.UtilCallService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClientPokeApi {

	@Value("${spring.client.poke-api-get-pokemon}")
	private String urlGetPokemon;

	@Value("${spring.client.poke-api-get-characteristics}")
	private String urlGetPokemonDescription;

	@Value("${spring.client.poke-api-get-evolutions}")
	private String urlGetPokemonEvolutions;

	@Autowired
	RestTemplate restTemplate;

	public ResponseEntity<Pokemon> getPokemon(Integer id) throws BusinessException {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlGetPokemon.concat(id.toString()));
		log.info("Request to get pokemon with id {} sent.", id);
		ResponseEntity<Pokemon> response = UtilCallService.consumeService(restTemplate, builder.toUriString(),
				new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get pokemon finalized.");
		return response;

	}

	public ResponseEntity<PokemonDescription> getPokemonDescription(Integer id) throws BusinessException {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlGetPokemonDescription.concat(id.toString()));
		log.info("Request to get description pokemon with id {} sent.", id);
		ResponseEntity<PokemonDescription> response = UtilCallService.consumeServiceDescription(restTemplate,
				builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get description pokemon finalized.");
		return response;

	}

	public ResponseEntity<EvolutionChains> getPokemonEvolutions(String id) throws BusinessException {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(id);
		log.info("Request to get evolutions pokemon  sent.");
		ResponseEntity<EvolutionChains> response = UtilCallService.consumeServiceEvolutions(restTemplate,
				builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get evolutions pokemon finalized.");
		return response;

	}

	public ResponseEntity<PokemonSpecie> getPokemonSpecie(String url) throws BusinessException {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		log.info("Request to get specie pokemon  sent.");
		ResponseEntity<PokemonSpecie> response = UtilCallService.consumeServiceSpecie(restTemplate,
				builder.toUriString(), new HttpHeaders(), HttpMethod.GET);
		log.info("Request to get specie pokemon finalized.");
		return response;

	}

}
