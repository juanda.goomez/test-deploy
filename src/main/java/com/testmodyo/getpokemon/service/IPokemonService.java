package com.testmodyo.getpokemon.service;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

public interface IPokemonService {

	public PokemonDto getPokemons(int page) throws BusinessException;

	public PokemonDetailDto getPokemonDetail(int id) throws BusinessException;

	public Pokemon getPokemon(int id) throws BusinessException;

	public PokemonDescription getPokemonDescription(int id) throws BusinessException;

	public EvolutionChains getPokemonEvolutions(String id) throws BusinessException;

}
