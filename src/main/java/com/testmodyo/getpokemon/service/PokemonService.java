package com.testmodyo.getpokemon.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.external.ClientPokeApi;
import com.testmodyo.getpokemon.mapper.IMapperService;
import com.testmodyo.getpokemon.models.domain.Chain;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PokemonService implements IPokemonService {

	@Autowired
	ClientPokeApi clientPokeApi;

	@Autowired
	IMapperService iMapperService;

	@Autowired
	CacheService cacheService;

	@Override
	public PokemonDto getPokemons(int id) throws BusinessException {
		Pokemon pokemon = callServicePokeApi(id);
		return iMapperService.pokemonToPokemonDto(pokemon);

	}

	private Pokemon callServicePokeApi(int id) throws BusinessException {

		try {
			return getPokemon(id);
		} catch (BusinessException e) {
			if (e.getStatus() == 404) {
				e.setDescription(CommonConstants.MESSAGE_NOT_FOUND_POKEMON);
				e.setCode(CommonConstants.RESOURCE_NOT_FOUND);

			}
			throw e;
		}

	}

	@Override
	public Pokemon getPokemon(int id) throws BusinessException {
		Pokemon pokemon = cacheService.getPokemon(id);
		if (pokemon == null) {
			pokemon = clientPokeApi.getPokemon(id).getBody();
			cacheService.savePokemon(pokemon);
		}
		return pokemon;
	}

	@Override
	public PokemonDetailDto getPokemonDetail(int id) throws BusinessException {
		Pokemon pokemon = null;
		try {
			pokemon = getPokemon(id);
		} catch (BusinessException e) {
			if (e.getStatus() == 404) {
				e.setDescription(CommonConstants.MESSAGE_NOT_FOUND_POKEMON);
				e.setCode(CommonConstants.RESOURCE_NOT_FOUND);
			}
			throw e;
		}
		PokemonDescription pokemonDescription = null;
		try {
			pokemonDescription = getPokemonDescription(id);
		} catch (BusinessException e) {
			log.warn(CommonConstants.MESSAGE_NOT_FOUND_DESCRIPTION_POKEMON, id);
		}
		String description = pokemonDescription != null ? pokemonDescription.getDescriptions().get(2).getDescription()
				: CommonConstants.DEFAULT_NOT_DESCRIPTION;

		ResponseEntity<PokemonSpecie> pokemonSpecie = clientPokeApi.getPokemonSpecie(pokemon.getSpecies().getUrl());
		EvolutionChains evolutions = getPokemonEvolutions(pokemonSpecie.getBody().getEvolutionChain().getUrl());
		PokemonDto pokemonDto = iMapperService.pokemonToPokemonDto(pokemon);
		return iMapperService.pokemonDtoToPokemonDetailDto(pokemonDto, getPokemonsEvolutions(evolutions, id),
				description);

	}

	private List<PokemonDto> getPokemonsEvolutions(EvolutionChains evolutions, int id) throws BusinessException {
		List<PokemonDto> pokemonsEvolutions = new LinkedList<>();
		List<Chain> chains = evolutions.getChain().getEvolvesTo();
		for (Chain chain : chains) {
			addPokemonEvolution(id, chain, pokemonsEvolutions);
			chains = chain.getEvolvesTo();
			if (chains.get(0).getEvolvesTo().isEmpty()) {
				addPokemonEvolution(id, chains.get(0), pokemonsEvolutions);

			}
		}
		return pokemonsEvolutions;
	}

	private void addPokemonEvolution(int id, Chain chain, List<PokemonDto> pokemonsEvolutions)
			throws BusinessException {
		String[] aux = chain.getSpecies().getUrl().split("/");
		Integer index = Integer.valueOf(aux[aux.length - 1]);
		if (index != id)
			pokemonsEvolutions.add(iMapperService.pokemonToPokemonDto(getPokemon(index)));
	}

	@Override
	public PokemonDescription getPokemonDescription(int id) throws BusinessException {
		PokemonDescription pokemonDescription = cacheService.getPokemonDescription(id);
		if (pokemonDescription == null) {
			pokemonDescription = clientPokeApi.getPokemonDescription(id).getBody();
			cacheService.savePokemonDescription(id, pokemonDescription);
		}
		return pokemonDescription;
	}

	@Override
	public EvolutionChains getPokemonEvolutions(String id) throws BusinessException {
		EvolutionChains evolutionChains = cacheService.getPokemonEvolutions(id);
		if (evolutionChains == null) {
			evolutionChains = clientPokeApi.getPokemonEvolutions(id).getBody();
			cacheService.savePokemonEvolutions(id, evolutionChains);
		}
		return evolutionChains;
	}

}
