package com.testmodyo.getpokemon.service;

import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;

public interface ICacheService {

	void savePokemon(Pokemon pokemon);

	Pokemon getPokemon(Integer id);

	void savePokemonDescription(Integer id, PokemonDescription pokemonDescription);

	PokemonDescription getPokemonDescription(Integer id);

	void savePokemonEvolutions(String id, EvolutionChains evolutionChains);

	EvolutionChains getPokemonEvolutions(String id);

}
