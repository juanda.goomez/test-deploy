package com.testmodyo.getpokemon.service;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;

@Service
public class CacheService implements ICacheService {

	private ConcurrentHashMap<Integer, Pokemon> pokemons;
	private ConcurrentHashMap<Integer, PokemonDescription> pokemonDescriptions;
	private ConcurrentHashMap<String, EvolutionChains> evolutions;

	public CacheService() {
		pokemons = new ConcurrentHashMap<>();
		pokemonDescriptions = new ConcurrentHashMap<>();
		evolutions = new ConcurrentHashMap<>();
	}

	@Override
	public void savePokemon(Pokemon pokemon) {
		pokemons.put(pokemon.getId(), pokemon);
	}

	@Override
	public Pokemon getPokemon(Integer id) {
		return pokemons.get(id);
	}

	@Override
	public void savePokemonDescription(Integer id, PokemonDescription pokemonDescription) {
		pokemonDescriptions.put(id, pokemonDescription);

	}

	@Override
	public PokemonDescription getPokemonDescription(Integer id) {

		return pokemonDescriptions.get(id);
	}

	@Override
	public void savePokemonEvolutions(String id, EvolutionChains evolutionChains) {
		evolutions.put(id, evolutionChains);

	}

	@Override
	public EvolutionChains getPokemonEvolutions(String id) {
		return evolutions.get(id);
	}

}
