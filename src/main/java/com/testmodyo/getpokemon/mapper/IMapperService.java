package com.testmodyo.getpokemon.mapper;

import java.util.List;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

public interface IMapperService {

	public PokemonDto pokemonToPokemonDto(Pokemon pokemon) throws BusinessException ;

	public PokemonDetailDto pokemonDtoToPokemonDetailDto(PokemonDto pokemon, List<PokemonDto> evolutions,
			String description) throws BusinessException ;

}
