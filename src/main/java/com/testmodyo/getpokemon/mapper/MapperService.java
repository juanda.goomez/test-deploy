package com.testmodyo.getpokemon.mapper;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.AbilityDetail;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.TypeDetail;
import com.testmodyo.getpokemon.models.dto.PokemonDetailDto;
import com.testmodyo.getpokemon.models.dto.PokemonDto;

@Service
public class MapperService implements IMapperService {

	@Override
	public PokemonDto pokemonToPokemonDto(Pokemon pokemon) throws BusinessException  {
		List<String> abilities = new LinkedList<>();
		for (AbilityDetail ability : pokemon.getAbilities()) {
			abilities.add(ability.getAbility().getName());
		}
		List<String> types = new LinkedList<>();

		for (TypeDetail type : pokemon.getTypes()) {
			types.add(type.getType().getName());
		}
		return PokemonDto.builder().id(pokemon.getId()).weight(pokemon.getWeight()).name(pokemon.getName())
				.abilities(abilities).photo(pokemon.getSprites().getBackDefault()).types(types).build();
	}

	@Override
	public PokemonDetailDto pokemonDtoToPokemonDetailDto(PokemonDto pokemon, List<PokemonDto> evolutions,
			String description) throws BusinessException  {
		return PokemonDetailDto.builder().pokemon(pokemon).evolutions(evolutions).description(description).build();

	}

}
