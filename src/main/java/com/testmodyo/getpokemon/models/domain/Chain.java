package com.testmodyo.getpokemon.models.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class Chain implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("evolves_to")
	private List<Chain> evolvesTo;
	private Specie species;

}
