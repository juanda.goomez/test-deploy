package com.testmodyo.getpokemon.models.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class ResponseDto<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code;
	private String description;
	private int status;
	@JsonProperty("response_body")
	private T responseBody;

	public ResponseDto() {
		super();

	}

	public ResponseDto(String code, String description, int status, T response) {
		super();
		this.code = code;
		this.description = description;
		this.status = status;
		this.responseBody = response;
	}

}
