package com.testmodyo.getpokemon.models.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.Generated;

@Generated
@Data
@Builder
public class PokemonDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String photo;
	private List<String> types;
	private List<String> abilities;
	private Integer weight;

}
