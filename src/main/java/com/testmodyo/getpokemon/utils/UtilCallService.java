package com.testmodyo.getpokemon.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import com.testmodyo.getpokemon.common.CommonConstants;
import com.testmodyo.getpokemon.controller.exception.BusinessException;
import com.testmodyo.getpokemon.models.domain.EvolutionChains;
import com.testmodyo.getpokemon.models.domain.Pokemon;
import com.testmodyo.getpokemon.models.domain.PokemonDescription;
import com.testmodyo.getpokemon.models.domain.PokemonSpecie;

public class UtilCallService {

	private UtilCallService() {

	}

	public static ResponseEntity<Pokemon> consumeService(RestTemplate restTemplate, String url, HttpHeaders headers,
			HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), Pokemon.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {		
			throw new BusinessException(CommonConstants.CLIENT_ERROR, e.getMessage(), e.getRawStatusCode());
		}
	}

	public static ResponseEntity<PokemonDescription> consumeServiceDescription(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), PokemonDescription.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR, e.getMessage(), e.getRawStatusCode());
		}
	}

	public static ResponseEntity<EvolutionChains> consumeServiceEvolutions(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), EvolutionChains.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR, e.getMessage(), e.getRawStatusCode());
		}
	}
	
	public static ResponseEntity<PokemonSpecie> consumeServiceSpecie(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) throws BusinessException {
		try {
			return restTemplate.exchange(url, method, new HttpEntity<>(headers), PokemonSpecie.class);
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			throw new BusinessException(CommonConstants.CLIENT_ERROR, e.getMessage(), e.getRawStatusCode());
		}
	}

}
